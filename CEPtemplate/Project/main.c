/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include <stdint.h>
#include "CE_Lib.h"
#include "tft.h"
#include "flash.h"
#include "prbsutil.h"

// config spi mode
#define SPI_MODE_3          (SPI_CPOL_High | SPI_CPHA_2Edge)    // set spi mode 3 for AT25DF641 ()
#define SPI_CLK_DIV_2       SPI_BaudRatePrescaler_2             // 

// macro converts binary value (containing up to 8 bits resp. <=0xFF) to unsigned decimal value
// as substitute for missing 0b prefix for binary coded numeric literals.
// macro does NOT check for an invalid argument at all.
#define b(n) (                                               \
    (unsigned char)(                                         \
    ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
    |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
    |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
    )

// simplified acces to switches S0 - S7
#define  S1   ( !(GPIOH->IDR & (1 << 15)) )
#define  S2   ( !(GPIOH->IDR & (1 << 12)) )
#define  S3   ( !(GPIOH->IDR & (1 << 10)) )
#define  S4   ( !(GPIOF->IDR & (1 << 8 )) )
#define  S5   ( !(GPIOF->IDR & (1 << 7 )) )
#define  S6   ( !(GPIOF->IDR & (1 << 6 )) )
#define  S7   ( !(GPIOC->IDR & (1 << 2 )) )
#define  S8   ( !(GPIOI->IDR & (1 << 9 )) )
//----------------------------------------------------
//================READ==START=========================
//----------------------------------------------------
void read(enum Chip type,uint32_t address, size_t nob){
    uint8_t receiveByte = DUMMY;
    int i;
	uint32_t rc;
    if(address <= MAX_ADDRESS){
		assert(type);
        //sending Op code for READ 
        receiveByte = spi_write_byte(READ_FAST) ;
        //sending the starting address
        receiveByte = send_add(address);   
        //2xDummy transmission to get started
        receiveByte = spi_write_byte(DUMMY); 
        receiveByte = spi_write_byte(DUMMY);
        //The actual reading starts here..
        for(i=0;i < nob;i++){
            SPI3->DR = DUMMY;            
            while(!(SPI3->SR & SPI_SR_RXNE));
            receiveByte = SPI3->DR;
#ifdef PRBS
            rc = chkNextByteOutOfPRBSstream( receiveByte );  chk_rc( rc );
#else
            //printf("0x%08x\n", receiveByte); 
			printf("%d\n", receiveByte); 
#endif
        }//finished reading
#ifdef PRBS   
        printf( "rc = %d,  i = %d\n",  rc, i );
#endif
        //disconnect
	deassert();
    }
}
//----------------------------------------------------
//================READ==END===========================
//----------------------------------------------------

//----------------------------------------------------
//===============ERASE==START=========================
//----------------------------------------------------
void erase_xKB(enum Chip type, uint32_t address, uint8_t op_code){
	uint8_t receiveByte;
	write_enable(type);
	unprotect_sector(type, address);
	write_enable(type);
	assert(type);
    receiveByte = spi_write_byte(op_code);
    receiveByte = send_add(address) ;
	deassert();
    wait_unitl_rdy(type);
}

void wait_unitl_rdy(enum Chip type)
{
	uint8_t result = 0;
	assert(type);
	spi_write_byte(READ_STATUS_REG);
	do{
		//result = spi_write_byte(DUMMY)<<8;
        result = spi_write_byte(DUMMY);
	}while(result & 0x01);
	deassert();
}

int checkForValidBeginning(uint32_t address)
{
    if((address <= 0x7FF000) && ((address & 0xfff) == 0x0))
    {
        return 1;
    }
    return 0;
}

int checkForValidSize(size_t erase_size)
{
    if ((erase_size <= MAX_SIZE) && !(erase_size % _4KB))
    {
        return erase_size;
    }
    if((erase_size <= MAX_ADDRESS) && ((erase_size & 0xfff) == 0xfff))
    {
        return erase_size+1;
    }
    return 0;
}

int validSuffix(uint32_t address, uint32_t suffix)
{
    if ((address & 0xFFFF) == suffix)
    {
        return 1;
    }
    return 0;
}

void erase_logic(enum Chip type, uint32_t address, size_t erase_size) {
    size_t temp_size;
    uint8_t save_flashy = 0;
    if(MAX_ADDRESS < erase_size){
        erase_xKB(type, address, ERASE_CHIP);        
    }else{
        if (checkForValidBeginning(address) && (temp_size = checkForValidSize(erase_size))){
            while (0 < temp_size && save_flashy < MAX_ERASE_OPS ) {
                if (_64KB <= temp_size && validSuffix(address, VALID_64KB_SUFFIX)) {
                    erase_xKB(type, address, ERASE_64KB);
                    address = ((address | _64KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _64KB;
                }
                else if (_32KB <= temp_size && (validSuffix(address, VALID_32KB_SUFFIX) || validSuffix(address, 0x0)))
                {
                    erase_xKB(type, address, ERASE_32KB);
                    address = ((address | _32KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _32KB;
                }
                else
                {
                    erase_xKB(type, address, ERASE_4KB);
                    address = ((address | _4KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _4KB;
                }
                save_flashy++;
            }  
        }        
    }
}

//----------------------------------------------------
//===============ERASE==END===========================
//----------------------------------------------------

//----------------------------------------------------
//===========PROGRAM==START===========================
//----------------------------------------------------
void program_interval(enum Chip type, uint32_t address, size_t length, uint8_t* array){
    int i;
    uint8_t receiveByte;
	uint32_t currentIndex = 0;
    erase_logic(type,address,length);
    while(length){
		write_enable(type);
		unprotect_sector(type, address);
		write_enable(type);	
        //sending the programming code and the address to start writing..
		assert(type);
        receiveByte = spi_write_byte(BYTE_PAGE_PROGRAM);
        receiveByte = send_add(address);
        for(i = 0; (i < PAGE_SIZE)&& (length!= 0);i++){
#ifdef PRBS          
            receiveByte = getNextByteOutOfPRBSstream();
            spi_write_byte(receiveByte);       
#else            
			printf("%d\n",array[currentIndex + i]);
            spi_write_byte(array[currentIndex + i]);
			currentIndex++;
#endif
            length--;
        }
        //unplugging
		deassert();	
        //waiting for the procedure to finish
		wait_unitl_rdy(type);
        address = (address | _256B_MASK) + 1;
    }     
}

void program(enum Chip type,uint32_t address, size_t length, uint8_t* array){
    if(address <= MAX_ADDRESS){
            uint32_t difference;
            difference = MAX_ADDRESS - address; //calculate possible space from given address til MAX_ADDRESS
            if(difference  < length){ //check if required space is given
                program_interval(type,address, difference, array); 
                program_interval(type,address, MIN_ADDRESS, array + length - difference);                
            }else{
                program_interval(type, address, length, array);                
            }
    }
}

void programA4(void){
    int i;
    enum Chip type;
	    uint32_t address;
    uint32_t buffer;
    size_t length = 0;
    uint8_t array[PAGE_SIZE];
    for(i = 0; i < PAGE_SIZE; i++ ){
        array[i] = 0;
    }
    address = 0;
    buffer = 0;
    printf("Please enter 0 to select ORIGINAL or 1 to select WORK chip..\n");
    fflush(stdout);
    buffer = inputHandler();
    if(buffer){type = WORK;}else{type = ORIGINAL;}
    printf("Please enter the starting address in hex format..\n");  
    fflush(stdout);
    address = inputHandler();   
    printf("Input numbers one by one (hex), confirm each with enter,0xffffffff will stop the sequence\n");
    fflush(stdout);
    buffer = inputHandler();   
    for(i = 0;i < PAGE_SIZE && buffer!= KEVIN_CONSTANT;i++ ){
        array[i] = buffer;
        buffer = inputHandler(); 
        length++;
    }
    program(type, address, length, array);
}
//----------------------------------------------------
//===========PROGRAM====END===========================
//----------------------------------------------------

//----------------------------------------------------
//===========PRBS===START=============================
//----------------------------------------------------
void programPRBS(void){
    uint8_t dummy;
    enum Chip type;
    size_t length = 0;
    uint32_t address = 0;
    uint32_t buffer = 0;
    printf("Please enter 0 to select WORK or 1 to select ORIGINAL chip..\n");
    fflush(stdout);
    buffer = inputHandler();
    if(buffer){type = WORK;}else{type = ORIGINAL;}
    printf("Please enter the starting address in hex format..\n");  
    fflush(stdout);
    address = inputHandler();   
    printf("Please enter the NumberOfElements for the PRBS sequence..\n");
    fflush(stdout);
    length = inputHandler();   
    dummy = 0;
    program(type, address, length, &dummy);
}
void readPRBS(void){
    enum Chip type;
    size_t length = 0;
    uint32_t address = 0;
    uint32_t buffer = 0;
    printf("Please enter 0 to select ORIGINAL or 1 to select WORK chip..\n");
    fflush(stdout);
    buffer = inputHandler();
    if(buffer){type = WORK;}else{type = ORIGINAL;}
    printf("Please enter the starting address in hex format..\n");  
    fflush(stdout);
    address = inputHandler();   
    printf("Please enter the NumberOfElements for the PRBS sequence..\n");
    fflush(stdout);
    length = inputHandler();   
    read(type, address, length);
}
//----------------------------------------------------
//===========PRBS===END===============================
//----------------------------------------------------

//utility functions
uint8_t spi_write_byte(uint8_t data) {
    SPI3->DR = data;                    // write dummy byte into data register
    while(!(SPI3->SR & SPI_SR_RXNE));   // wait until valid data is in rx buffer (RM0090 Chap 28.3.7)
    return SPI3->DR;                    // read data register (RM0090 Chap 28.3.7)
}

void write_enable(enum Chip type){
    assert(type);
    spi_write_byte(WRITE_ENABLE) ;
	deassert();
}

void unprotect_sector(enum Chip type, uint32_t address){
    assert(type);
    spi_write_byte(UNPROTECT_SECTOR) ;
    send_add(address) ;
	deassert();
}

uint32_t inputHandler(){
    uint32_t input;
    printf("\nEnter a hex number: ");
    fflush(stdout);
    scanf ("%x",&input);
    return input;
}
uint8_t send_add(uint32_t address){
    uint8_t receiveByte;
    receiveByte = spi_write_byte(address>>16) ; //MSB->LSB,sending 1st address byte      
    receiveByte = spi_write_byte(address>>8);//MSB->LSB,sending 2nd address byte 
    receiveByte = spi_write_byte(address);//MSB->LSB,sending 3rd address byte 
    return receiveByte;
}

void manufacturer_info(enum Chip type){
    uint32_t dev_info;
	assert(type);
    spi_write_byte(READ_DEV_ID);
    dev_info =(
        spi_write_byte(DUMMY)         |
        (spi_write_byte(DUMMY) << 8)  |
        (spi_write_byte(DUMMY) << 16) |
        (spi_write_byte(DUMMY) << 24)
    );
	deassert();   
    printf("Device Info:%d\n", dev_info);    
}

void assert(enum Chip type){
     if(type == WORK){
         GPIOB->BSRRH = GPIO_Pin_9;
     }else{
         GPIOG->BSRRH = GPIO_Pin_6;
     }
}

void deassert(void){
    GPIOB->BSRRL = GPIO_Pin_9;
    GPIOG->BSRRL = GPIO_Pin_6;
}

void showMenu(void){
    printf("====MENU_OPTIONS====\n");
    printf("0: SELECT_CHIP\n");
    printf("1: SHOW_MANUFACTURER_ID\n");
    printf("2: READ_MEMORY\n");
    printf("3: DELETE_MEMORY\n");
    printf("4: PROGRAM_MEMORY\n");
    printf("5: PROGRAM_PRBS\n");
    printf("6: READ_PRBS\n");
}

void select_chip_type(uint8_t *chip){
    printf("PLEASE CHOOSE CHIP TYPE\n");
    printf("0: WORK\n1: ORIGINAL\n");
    while(*(chip) != WORK && *(chip) != ORIGINAL){
        *(chip) = inputHandler();
        if(*(chip) != WORK && *(chip) != ORIGINAL)
        {
            printf("NO VALID CHIP TYPE\n");
        }
    }
}
    
uint32_t get_start_address(){
    printf("TYPE IN START ADDRESS\n");
    return inputHandler();
}

size_t get_size(){
    printf("TYPE IN SIZE\n");
    return inputHandler();
}
//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void ){
    enum Chip type;
	uint8_t receiveByte;
    uint8_t test[10]={16,0,2,1,9,9,1,7,8,9};
	static uint8_t chip = 0xff;
    // general setup
    // =============
    //
    initCEP_Board();                            

    // clock setup (enable clock for used ports)
    // =============
    //
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // enable clock for GPIOG
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI

    // SPI
    // =====
    //
    // Set PortB.9 as output (Chip Select 1)
    GPIOB->MODER |= (GPIO_Mode_OUT << (2*9)); 
    // Set PortG.6 as output (Chip Select 2)
    GPIOG->MODER |= (GPIO_Mode_OUT << (2*6));
    
    // Set SPI Pins to alternate port function
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
    GPIOC->MODER |= (GPIO_Mode_AF << (2*10)) | (GPIO_Mode_AF << (2*11)) | (GPIO_Mode_AF << (2*12));
    GPIOC->OSPEEDR |= (GPIO_Fast_Speed << (2*10)) | (GPIO_Fast_Speed << (2*11)) | (GPIO_Fast_Speed << (2*12));
    // set alternate function mode for use with spi (RM0090 Chap 8.3.2)
    GPIOC->AFR[1] |= (GPIO_AF_SPI3 << (4*2)) | (GPIO_AF_SPI3 << (4*3)) | (GPIO_AF_SPI3 << (4*4));
    
    // Set SPI Configuration
    // enable clock for SPI (RM0090 Chap 6.3.13)
    RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
    // set SPI configuration(RM0090 Chap 28.5.1)    
    SPI3->CR1 = (SPI_CR1_SPE | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CLK_DIV_2 | SPI_MODE_3);

    // disable chip selects
    deassert();
	
    select_chip_type(&chip);
    showMenu();
    while(1)
    {
        switch(inputHandler()){
            case SELECT_CHIP: select_chip_type(&chip); break;
            case SHOW_MANUFACTURER_ID: manufacturer_info(chip); break;
            case READ_MEMORY: read(chip, get_start_address(), get_size()); break;
            case DELETE_MEMORY: erase_logic(chip, get_start_address(), get_size()); break;
            case PROGRAM_MEMORY: programA4(); break;
            case PROGRAM_PRBS: programPRBS(); break;
            case READ_PRBS: readPRBS(); break;
            default: printf("NO VALID MENU NUMBER\n");
        }
		showMenu();
    }

//    printf("\n\n======START======\n\n");
//    printf("====PROGRAMMING...====\n");
//	program(WORK,0x0, 0xA, test);
//	printf("====READ_PROGRAMMED====\n");
//	read(WORK,0x0,0xA);
//	printf("====ERASE...====\n");
//	erase_logic(WORK, 0x0, _4KB);
//	printf("====READ_ERASED====\n");
//	read(WORK,0x0,0xA);
//	printf("\n===FINISHED===\n");
    
    while(1){};

}//main


