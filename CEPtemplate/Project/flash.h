/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */
#ifndef __FLASH
#define __FLASH

#include <stdint.h>

//Read Commands
#define READ_FAST 0x1b // 2 dummy bytes required
#define READ_MEDIUM 0x0b// 1 dummy byte required
#define READ_SLOW 0x03
#define D_READ_FAST 0x3b // 1 dummy byte required

//Program Commands
#define BYTE_PAGE_PROGRAM 0x02
#define DI_BYTE_PAGE_PROGRAM 0xa2

//Erase Commands
#define ERASE_4KB 0x20
#define ERASE_32KB 0x52
#define ERASE_64KB 0xd8
#define ERASE_CHIP 0x60

//Protection Commands
#define WRITE_ENABLE 0x06
#define WRITE_DISABLE 0x04
#define PROTECT_SECTOR 0x36
#define UNPROTECT_SECTOR 0x39
#define R_PROTECTION_REG 0x3c

//Status Register Commands
#define READ_STATUS_REG 0x05

//Misc Commands
#define READ_DEV_ID 0x9f

// Useful constants
#define KEVIN_CONSTANT 0xffffffff
#define  MAX_ERASE_OPS   156
#define _256B_MASK 0xff
#define PAGE_SIZE 256
#define MIN_ADDRESS 0
#define MAX_ADDRESS 0x7fffff
#define DUMMY 0xff
#define _4KB 0x1000
#define _32KB 0x8000
#define _64KB 0x10000
#define _4KB_MASK 0xfff
#define _32KB_MASK 0x7fff
#define _64KB_MASK 0xffff
#define MAX_SIZE 0x80000
#define VALID_64KB_SUFFIX 0x0
#define VALID_32KB_SUFFIX 0x8000

#define PRBS
//Function prototypes and Enums
enum Chip { 
    WORK, 
    ORIGINAL
    };

void read(enum Chip type,uint32_t address, size_t nob);
void erase_logic(enum Chip type,uint32_t address, size_t erase_size);
void erase_xKB(enum Chip type, uint32_t address, uint8_t op_code);

void programA4(void);

void program(enum Chip type,uint32_t address, size_t length, uint8_t* array);
void program_interval(enum Chip type,uint32_t address, size_t length, uint8_t* array);

void programPRBS(void);

void readPRBS(void);

int validSuffix(uint32_t address, uint32_t suffix);
int checkForValidSize(size_t erase_size);
int checkForValidBeginning(uint32_t address);
uint8_t spi_write_byte(uint8_t data);
uint8_t send_add(uint32_t address);

uint32_t inputHandler(void);

void manufacturer_info(enum Chip type);

void assert(enum Chip type);
void deassert(void);
void wait_unitl_rdy(enum Chip type);
void write_enable(enum Chip type);
void unprotect_sector(enum Chip type, uint32_t address);

void showMenu(void);
void select_chip_type(uint8_t *chip);
uint32_t get_start_address(void);
size_t get_size(void);

enum Menu { 
    SELECT_CHIP,
    SHOW_MANUFACTURER_ID,
    READ_MEMORY,
    DELETE_MEMORY,
    PROGRAM_MEMORY,
    PROGRAM_PRBS,
    READ_PRBS
    };
#endif//_FLASH
